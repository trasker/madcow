round_of = lambda x: round(x/2.5)*2.5

monday = [0.5, 0.625, 0.75, 0.875, 1]  # 5x5
wedensday = [0.5, 0.625, 0.75, 0.75] # 4x5
friday = [0.5, 0.625, 0.75, 0.875, 1.025, 0.75]  # 5,5,5,5,3,8
bench = 80
squat = 80
ohp = 30
deadlift = 85
for i in range(1, 9):
    print(f"WEEK {i}")
    print("MONDAY:")
    sets = [round_of((i * 2.5 + bench) * s) for s in monday]
    print(f"bench: {sets} 5x5")
    sets = [round_of((i * 2.5 + squat) * s) for s in monday]
    print(f"squat: {sets} 5x5")
    sets = [round_of((i * 2.5 + ohp) * s) for s in monday[1:]]
    print(f"ohp: {sets} 4x5")
    print("WEDENSDAY:")
    sets = [round_of((i * 2.5 + bench) * s) for s in wedensday]
    print(f"bench: {sets} 4x5")
    sets = [round_of((i * 2.5 + squat) * s) for s in wedensday]
    print(f"squat: {sets} 4x5")
    sets = [round_of((i * 2.5 + ohp) * s) for s in wedensday]
    print(f"ohp: {sets} 4x5")
    print("FRIDAY:")
    sets = [round_of((i * 2.5 + bench) * s) for s in friday]
    print(f"bench: {sets} 5x4 + 1x3 + 1x8")
    sets = [round_of((i * 2.5 + squat) * s) for s in friday]
    print(f"squat: {sets} 5x4 + 1x3 + 1x8")
    sets = [round_of((i * 5 + ohp) * s) for s in monday[1:]]
    print(f"ohp: {sets} 4x5")
    sets = [round_of((i * 5 + deadlift) * s) for s in monday[1:]]
    print(f"deadlift: {sets} 4x5")
    print()
